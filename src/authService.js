const { User } = require('./models/Users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const createProfile = async (req, res, next) => {
  try {
    const { username, password } = req.body;

    if(username && password) {
      const user = new User({
        username,
        password: await bcrypt.hash(password, 10),
        createDate: req._startTime
      });

      user.save()
        .then(saved => res.status(200).json({
          "message": "Success"
        }))
        .catch(err => {
          next(err);
        });
    } else {
      res.status(400).json({
        "message": "string"
      });
    }
  } catch (error) {
    res.status(500).json({
      "message": "string"
    });
  }
}

const login = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
      const payload = { username: user.username, name: user.name, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secret-jwt-key');
      return res.json({
        "message": "Success",
        "jwt_token": jwtToken
      });
    } else {
      res.status(400).json({
        "message": "string"
      });
    }
  } catch (error) {
    res.status(500).json({
      "message": "string"
    });
  }
}

module.exports = {
  createProfile,
  login,
};
