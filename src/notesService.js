const { Notes } = require('./models/Notes.js');
const { notesRouter } = require('./notesRouter.js');

function addUserNotes(req, res, next) {
  try {
    const { text } = req.body;
    if(text) {
      const note = new Notes({
        text,
        userId: req.user.userId,
        createdDate: req._startTime
      });
      note.save().then(() => {
        res.json({
          "message": "Success"
        });
      });
    } else {
      res.status(400).json({
        "message": "string"
      });
    }
  } catch (error) {
    res.status(500).json({
      "message": "string"
    });
  }
}

const getNote = (req, res, next) => {
  try {
    if(req.url) {
      Notes.findById(req.params.id)
      .then((note) => {
        res.json({
          note: {
            _id: note._id,
            userId: note.userId,
            completed: note.completed,
            text: note.text,
            createdDate: note.createdDate
          }
        });
      });
    } else {
      res.status(400).json({
        "message": "string"
      });
    }
  } catch (error) {
    res.status(400).json({
      "message": "string"
    });
  }
};

const deleteNote = (req, res, next) => { 
  try {
    if(req.url) {
      Notes.findByIdAndDelete(req.params.id)
      .then(() => {
        res.json({
          "message": "Success"
        });
      });
    } else {
      res.status(400).json({
        "message": "string"
      });
    }
  } catch (error) {
    res.status(500).json({
      "message": "string"
    });
  }
}

const getUserNotes = (req, res, next) => {
  try {
    Notes.find({ userId: req.user.userId }, '-__v')
    .then((result) => {
      res.json({
        "offset": 0,
        "limit": 0,
        "count": 0,
        "notes": result
      });
    });
  } catch (error) {
    res.status(500).json({
      "message": "string"
    });
  }
};

const updateMyNoteById = (req, res, next) => {
  const { text } = req.body;
  return Notes.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
    .then((result) => {
      res.json({ message: 'note was updated' });
    });
};

const markMyNoteCompletedById = (req, res, next) => Notes.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { completed: true } })
  .then((result) => {
    res.json({ message: 'note was marked completed' });
  });

module.exports = {
  addUserNotes,
  getNote,
  deleteNote,
  getUserNotes,
  updateMyNoteById,
  markMyNoteCompletedById,
};
